const expect = require('expect');
const describe = require("mocha").describe;
const it = require("mocha").it;


describe('MasterMind', () => {
    const SUT = require('../sources/Mastermind');

    it('should be a class', () => {
        let sut = new SUT();
    });

    it('should respect the given interface', () => {
        let sut = new SUT();
        expect(sut.done).toNotBe(undefined);
        expect(sut.successful).toNotBe(undefined);
        expect(sut.results).toNotBe(undefined);
        expect(sut.remainingAttempts).toNotBe(undefined);
        expect(sut.attempts).toNotBe(undefined);
        expect(sut.guess).toNotBe(undefined);
    });

    describe('When a game start', () => {
        it('should have maximum attempts left', () => {
            const sequence = [1, 9, 3];
            const max_attempts = 10;

            let sut = new SUT(sequence, max_attempts);
            expect(sut.remainingAttempts).toBe(10)
        });

        it('should NOT be done', () => {
            const sequence = [1, 9, 3];
            const max_attempts = 10;

            let sut = new SUT(sequence, max_attempts);
            expect(sut.done).toBe(false);
        });

        it('should NOT be successful', () => {
            const sequence = [1, 9, 3];
            const max_attempts = 10;

            let sut = new SUT(sequence, max_attempts);
            expect(sut.successful).toBe(false);
        });

        it('should allow a player to try a sequence', () => {
            const sequence = [1, 9, 3];
            const max_attempts = 10;

            let sut = new SUT(sequence, max_attempts);
            expect(sut.guess([3, 4, 5, 2, 4]));
        });
    });


    describe('When a player tries sequences', () => {
        it('should be chainable', () => {
            const sequence = [1, 9, 3];
            const max_attempts = 10;

            let sut = new SUT(sequence, max_attempts);
            expect(sut.guess([3, 4, 5, 2, 4])
                .guess([3, 4, 5, 2, 4])
                .guess([3, 4, 5, 2, 4])
                .guess([3, 4, 5, 2, 4])
            )
        });

        it('should reduce the attempts left after each try', () => {
            const sequence = [1, 9, 3];
            const max_attempts = 10;

            let sut = new SUT(sequence, max_attempts);
            sut.guess([3, 4, 5, 2, 4])
                .guess([3, 4, 5, 2, 4])
                .guess([3, 4, 5, 2, 4])
                .guess([3, 4, 5, 2, 4]);

            expect(sut.remainingAttempts).toBe(6);
        });

        it('should keep references of each guess', () => {
            const sequence = [1, 9, 3];
            const max_attempts = 10;

            let sut = new SUT(sequence, max_attempts);
            sut.guess([3, 4, 5, 2, 4])
                .guess([3, 4, 5, 2, 4])
                .guess([3, 4, 5, 2, 4])
                .guess([3, 4, 5, 2, 4]);

            expect(sut.results.length).toBe(4)
        });

        it('should replace invalids guesses by -1', () => {
            const sequence = [1, 9, 3];
            const max_attempts = 10;

            let sut = new SUT(sequence, max_attempts);
            sut.guess([5, 5, 5]);

            expect(sut.results.last()).toEqual([-1, -1, -1])
        });

        it('should replace mispositioned guesses by 1', () => {
            const sequence = [1, 9, 3];
            const max_attempts = 10;

            let sut = new SUT(sequence, max_attempts);
            sut.guess([9, 3, 1]);

            expect(sut.results.last()).toEqual([1, 1, 1])
        });

        it('should replace good guesses by 0', () => {
            const sequence = [1, 9, 3];
            const max_attempts = 10;

            let sut = new SUT(sequence, max_attempts);
            sut.guess([1, 9, 3]);

            expect(sut.results.last()).toEqual([0, 0, 0])
        });
    });

    describe('When a player reaches the maximum attempts limit', () => {
        it('should ignore the next attempts', () => {
            const sequence = [1, 9, 3];
            const max_attempts = 1;

            let sut = new SUT(sequence, max_attempts);
            sut.guess([1, 9, 4])
                .guess([1, 9, 4])
                .guess([1, 9, 4]);

            expect(sut.attempts.length).toBe(1);
            expect(sut.results.length).toBe(1);
        });

        it('should update the `successful` state', () => {
            const sequence = [1, 9, 3];
            const max_attempts = 1;

            let sut = new SUT(sequence, max_attempts);
            sut.guess([1, 9, 4]);

            expect(sut.successful).toBe(false);
        });

        it('should update the `done` state', () => {
            const sequence = [1, 9, 3];
            const max_attempts = 1;

            let sut = new SUT(sequence, max_attempts);
            sut.guess([1, 9, 4]);

            expect(sut.done).toBe(true);
        });
    });

    describe('When a player find the hidden sequence', () => {
        it('should ignore the next attempts', () => {
            const sequence = [1, 9, 3];
            const max_attempts = 10;

            let sut = new SUT(sequence, max_attempts);
            sut.guess([1, 9, 3])
                .guess([1, 9, 4])
                .guess([1, 9, 4]);

            expect(sut.attempts.length).toBe(1);
            expect(sut.results.length).toBe(1);
        });

        it('should update the `successful` state', () => {
            const sequence = [1, 9, 3];
            const max_attempts = 10;

            let sut = new SUT(sequence, max_attempts);
            sut.guess([1, 9, 3]);

            expect(sut.successful).toBe(true);
        });

        it('should update the `done` state', () => {
            const sequence = [1, 9, 3];
            const max_attempts = 10;

            let sut = new SUT(sequence, max_attempts);
            sut.guess([1, 9, 3]);

            expect(sut.done).toBe(true);
        });
    });
});


Array.prototype.last = function () {
    if (this.length == 0)
        return undefined;

    return this[this.length - 1];
};