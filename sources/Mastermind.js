/**
 *
 * @description
 * Mastermind is the entry point to start a game.
 * You have to passe an array of int that represent the sequence designed by the master.
 * and the number of attempts given to a guesser.
 * @returns {Game} a new game
 *
 */
class Mastermind {
    constructor(sequence, max_attempts) {}

    /**
     *
     * @return {Array}
     */
    get attempts() {
        return undefined;
    }

    /**
     *
     * @return {boolean}
     */
    get done() {
        return undefined;
    }

    /**
     *
     * @return {boolean}
     */
    get successful() {
        return undefined;
    }

    /**
     *
     * @return {Array}
     */
    get results() {
        return undefined;
    }

    /**
     *
     * @return {number}
     */
    get remainingAttempts() {
        return undefined;
    }

    /**
     *
     * @param {Array} suit
     * @return {Game}
     */
    guess(suit) {
        return undefined;
    }
};

module.exports = Mastermind;